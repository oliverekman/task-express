const express = require('express');
const path = require('path');
const bodyParser = require("body-parser");
const HOST = '0.0.0.0';


const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.get('/', (req, res) => {
    return res.sendFile(path.join(__dirname, 'public', 'index.html'));
});
app.get('/contact', (req, res) => {
    return res.sendFile(path.join(__dirname, 'public', 'contact.html'));
});
app.get('/about', (req, res) => {
    return res.sendFile(path.join(__dirname, 'public', 'about.html'));
});
app.get('/menu', (req, res) => {
    return res.sendFile(path.join(__dirname, 'public', 'menu.html'));
});

app.get('/contact/send-mail', (req, res) => res.json(contact));


app.use(express.static(path.join(__dirname, 'public')));


const PORT = process.env.PORT || 5000;

app.post('/contact/send-email', function (req, res) {
    let contact = req.body.contactName + ' ' + req.body.contactEmail + ' - ' + 'Message:' + ' ' + req.body.contactMessage;
    
    res.send(contact + '// Submitted Successfully!');

});


app.listen(PORT, HOST);
console.log(`Server started on port ${PORT}`);
